package org.pickcellslab.pickcells.prefuse;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.modules.DefaultDocument;

@Module
public class MetaModelView implements DefaultDocument, MetaModelListener {

	//private Logger log = LoggerFactory.getLogger(MetaModelView.class);

	private JPanel view;
	private MetaToPrefuseGraph graph;
	private RadialMetaView rmv;
	private Set<MetaModel> current;
	
	private final DataAccess access;

	static final String MetaColumn = "Meta";


	//private final ScheduledExecutorService executor =  Executors.newSingleThreadScheduledExecutor();
	//private ScheduledFuture<?> future;

	public MetaModelView(UITheme theme, DataAccess access, DBViewHelp helper, NotificationFactory notifier) {
		this.access = access;
		this.access.addMetaListener(this);
		
		current = (Set)access.metaModel().getAll();
		graph = new MetaToPrefuseGraph();
		if(!current.isEmpty()){
			graph.traverse(current.iterator().next());
		}

		if(view == null){
			view = new JPanel();
			view.setName("MetaModel");
		}
		else
			view.removeAll();
		
		view.setLayout(new BorderLayout());
		rmv = RadialMetaView.createView(graph.getPrefuseGraph(), "name");
		ItemControl iControl = new ItemControl(theme, access, notifier);
		rmv.addControlListener(iControl);
		
		helper.registerSeriesConsumersFor(iControl);

		view.add("Center",RadialMetaView.createScene(rmv));

		view.revalidate();
		view.repaint();
	}




	@Override
	public Component getScene() {
		return view;
	}


	@Override
	public void metaEvent(RegeneratedItems meta, MetaChange evt) {

		List<MetaModel> m = meta.getTargets(MetaModel.class).collect(Collectors.toList());

		if(evt == MetaChange.CREATED){
			if(current.addAll(m)){			
				graph.update(m);
				rmv.redoLayout();
			}
		}
		else{
			graph.delete(m);
			current.removeAll(m);
		}
	}







	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/meta_icon.png"));
	}



	@Override
	public int preferredLocation() {
		return DefaultDocument.UP;
	}

	@Override
	public void start() {
	
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		view = null;
		graph = null;
		rmv = null;
		current = null;
	}




	@Override
	public String name() {
		return "MetaModel View";
	}

	









}
