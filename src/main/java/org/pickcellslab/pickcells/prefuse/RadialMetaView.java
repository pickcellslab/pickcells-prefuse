package org.pickcellslab.pickcells.prefuse;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.text.View;

import org.pickcellslab.foundationj.dbm.access.Meta;

import prefuse.Constants;
import prefuse.Display;
import prefuse.Visualization;
import prefuse.action.ActionList;
import prefuse.action.GroupAction;
import prefuse.action.ItemAction;
import prefuse.action.RepaintAction;
import prefuse.action.animate.ColorAnimator;
import prefuse.action.animate.PolarLocationAnimator;
import prefuse.action.animate.QualityControlAnimator;
import prefuse.action.animate.VisibilityAnimator;
import prefuse.action.assignment.ColorAction;
import prefuse.action.assignment.DataColorAction;
import prefuse.action.assignment.FontAction;
import prefuse.action.layout.graph.FruchtermanReingoldLayout;
import prefuse.activity.SlowInSlowOutPacer;
import prefuse.controls.Control;
import prefuse.controls.ControlAdapter;
import prefuse.controls.DragControl;
import prefuse.controls.FocusControl;
import prefuse.controls.HoverActionControl;
import prefuse.controls.PanControl;
import prefuse.controls.ToolTipControl;
import prefuse.controls.ZoomControl;
import prefuse.controls.ZoomToFitControl;
import prefuse.data.Graph;
import prefuse.data.Node;
import prefuse.data.Table;
import prefuse.data.Tuple;
import prefuse.data.event.TupleSetListener;
import prefuse.data.query.SearchQueryBinding;
import prefuse.data.search.PrefixSearchTupleSet;
import prefuse.data.search.SearchTupleSet;
import prefuse.data.tuple.DefaultTupleSet;
import prefuse.data.tuple.TupleSet;
import prefuse.render.AbstractShapeRenderer;
import prefuse.render.DefaultRendererFactory;
import prefuse.render.EdgeRenderer;
import prefuse.render.LabelRenderer;
import prefuse.util.ColorLib;
import prefuse.util.FontLib;
import prefuse.util.ui.JFastLabel;
import prefuse.util.ui.JSearchPanel;
import prefuse.util.ui.UILib;
import prefuse.visual.EdgeItem;
import prefuse.visual.VisualItem;
import prefuse.visual.expression.InGroupPredicate;
import prefuse.visual.sort.TreeDepthItemSorter;


/**
 * A {@link View} based on the prefuse demo to display the state of the {@link Meta}
 * <a href="http://jheer.org">jeffrey heer</a>
 */

@SuppressWarnings("serial")
public class RadialMetaView extends Display{


	private static final String tree = "tree";
	private static final String treeNodes = "tree.nodes";
	private static final String treeEdges = "tree.edges";
	private static final String linear = "linear";

	private LabelRenderer m_nodeRenderer;
	private EdgeRenderer m_edgeRenderer;

	private String m_label = "label";

	public RadialMetaView(Graph g, String label) {
		super(new Visualization());
		m_label = label;

		// -- set up visualization --
		m_vis.add(tree, g);
		m_vis.setInteractive(treeEdges, null, false);


		// -- set up renderers --
		m_nodeRenderer = new LabelRenderer(m_label);
		m_nodeRenderer.setRenderType(AbstractShapeRenderer.RENDER_TYPE_FILL);
		m_nodeRenderer.setHorizontalAlignment(Constants.LEFT);
		//m_nodeRenderer.setManageBounds(true);
		m_nodeRenderer.setRoundedCorner(20,20);
		m_nodeRenderer.setHorizontalPadding(5);
		m_nodeRenderer.setVerticalPadding(5);


		m_edgeRenderer = new LinkRenderer(Constants.EDGE_TYPE_CURVE, Constants.EDGE_ARROW_FORWARD);

		DefaultRendererFactory rf = new DefaultRendererFactory(m_nodeRenderer);
		rf.add(new InGroupPredicate(treeEdges), m_edgeRenderer);
		m_vis.setRendererFactory(rf);


		// -- set up processing actions --

		// colors
		ItemAction nodeColor = new NodeColorAction(treeNodes);
		ItemAction textColor = new TextColorAction(treeNodes);
		m_vis.putAction("textColor", textColor);

		//   ItemAction edgeColor = new ColorAction(treeEdges,
		//       VisualItem.STROKECOLOR, ColorLib.rgb(0,0,0));
		//    ItemAction edgeHeadColor = new ColorAction(treeEdges,
		//        VisualItem.FILLCOLOR, ColorLib.rgb(0,0,0));

		FontAction fonts = new FontAction(treeNodes, 
				FontLib.getFont("Tahoma", 10));
		fonts.add("ingroup('_focus_')", FontLib.getFont("Tahoma", 11));


		// -- Colors for each type of node
		int[] palette = {
				ColorLib.rgb(241,163,64),//Class
				ColorLib.rgb(166,217,106),//Key
				ColorLib.rgb(145,191,219),//Link
				ColorLib.rgb(255,255,120),//Tag
				ColorLib.rgb(204,255,0),//Function
				ColorLib.rgb(102,255,204),//MultiFunction
				ColorLib.rgb(255,100,148),//Filter
				ColorLib.rgb(100,100,100),//Traversal
				ColorLib.rgb(255,154,255),//Connections
				ColorLib.rgb(51,51,255)//Search,
		};

		DataColorAction fill = new DataColorAction(treeNodes, "type",
				Constants.NOMINAL,
				VisualItem.FILLCOLOR, 
				palette);
		m_vis.putAction("fill", fill);

		// -- Colors for each type of edge
		int[] paletteArrow = {ColorLib.rgb(146,0,0), ColorLib.rgb(0,0,0), ColorLib.rgb(0,0,0), ColorLib.rgb(1,52,83)};
		DataColorAction fillArrow = new DataColorAction(treeEdges, "type",
				Constants.NOMINAL,
				VisualItem.FILLCOLOR, 
				paletteArrow);
		m_vis.putAction("fillArrow", fillArrow);

		DataColorAction fillStroke = new DataColorAction(treeEdges, "type",
				Constants.NOMINAL,
				VisualItem.STROKECOLOR, 
				paletteArrow);
		m_vis.putAction("fillStroke", fillStroke);



		// recolor
		ActionList recolor = new ActionList();        
		recolor.add(nodeColor);
		recolor.add(textColor);        
		m_vis.putAction("recolor", recolor);

		// repaint
		ActionList repaint = new ActionList();
		repaint.add(fill);
		repaint.add(new RepaintAction());
		m_vis.putAction("repaint", repaint);

		// animate paint change
		ActionList animatePaint = new ActionList(400);
		animatePaint.add(new ColorAnimator(treeNodes));
		animatePaint.add(new RepaintAction());
		m_vis.putAction("animatePaint", animatePaint);

		// create the tree layout action
		//RadialTreeLayout treeLayout = new RadialTreeLayout(tree);
		FruchtermanReingoldLayout treeLayout = new FruchtermanReingoldLayout(tree);

		//treeLayout.setAngularBounds(-Math.PI/2, Math.PI);
		m_vis.putAction("treeLayout", treeLayout);

		
		
		// CollapsedSubtreeLayout subLayout = new CollapsedSubtreeLayout(tree);
		// m_vis.putAction("subLayout", subLayout);

		// create the filtering and layout
		ActionList filter = new ActionList();
		filter.add(new TreeRootAction(tree));
		filter.add(fonts);
		filter.add(treeLayout);
		// filter.add(subLayout);
		//filter.add(edgeColor);
		//filter.add(edgeHeadColor);

		filter.add(fillArrow);
		filter.add(fillStroke);

		filter.add(nodeColor);

		filter.add(fill);
		filter.add(textColor);

		m_vis.putAction("filter", filter);

		// animated transition
		ActionList animate = new ActionList(1250);
		animate.setPacingFunction(new SlowInSlowOutPacer());
		animate.add(new QualityControlAnimator());
		animate.add(new VisibilityAnimator(tree));
		animate.add(new PolarLocationAnimator(treeNodes, linear));
		animate.add(new ColorAnimator(treeNodes));
		animate.add(new RepaintAction());
		m_vis.putAction("animate", animate);
		m_vis.alwaysRunAfter("filter", "animate");

		// ------------------------------------------------

		// initialize the display
		setSize(600,600);
		setItemSorter(new TreeDepthItemSorter());		
		addControlListener(new DragControl());
		addControlListener(new ZoomToFitControl(Control.MIDDLE_MOUSE_BUTTON));
		addControlListener(new ZoomControl());
		addControlListener(new PanControl());
		addControlListener(new FocusControl(1, "filter"));
		addControlListener(new HoverActionControl("repaint"));
		addControlListener(new ToolTipControl("description"));

		// ------------------------------------------------

		// filter graph and perform layout
		m_vis.run("filter");

		// maintain a set of items that should be interpolated linearly
		// this isn't absolutely necessary, but makes the animations nicer
		// the PolarLocationAnimator should read this set and act accordingly
		m_vis.addFocusGroup(linear, new DefaultTupleSet());
		m_vis.getGroup(Visualization.FOCUS_ITEMS).addTupleSetListener(
				new TupleSetListener() {
					public void tupleSetChanged(TupleSet t, Tuple[] add, Tuple[] rem) {
						TupleSet linearInterp = m_vis.getGroup(linear);
						if ( add.length < 1 ) return; linearInterp.clear();
						for ( Node n = (Node)add[0]; n!=null; n=n.getParent() )
							linearInterp.addTuple(n);
					}
				}
				);

		SearchTupleSet search = new PrefixSearchTupleSet();
		m_vis.addFocusGroup(Visualization.SEARCH_ITEMS, search);
		search.addTupleSetListener(new TupleSetListener() {
			public void tupleSetChanged(TupleSet t, Tuple[] add, Tuple[] rem) {
				m_vis.cancel("animatePaint");
				m_vis.run("recolor");
				m_vis.run("animatePaint");
			}
		});

	}
	
	
	public void redoLayout(){
		m_vis.run("filter");
		
	}
	
	

	// ------------------------------------------------------------------------

	public static RadialMetaView createView(Graph g, final String label){	
		// create a new radial tree view
		final RadialMetaView gview = new RadialMetaView(g, label);	
		
		return gview;
	}

	public static JPanel createScene(RadialMetaView gview) {        

		Visualization vis = gview.getVisualization();

		// create a search panel for the tree map
		SearchQueryBinding sq = new SearchQueryBinding(
				(Table)vis.getGroup(treeNodes), gview.m_label,
				(SearchTupleSet)vis.getGroup(Visualization.SEARCH_ITEMS));

		JSearchPanel search = sq.createSearchPanel();
		search.setShowResultCount(true);
		search.setBorder(BorderFactory.createEmptyBorder(5,5,4,0));
		search.setFont(FontLib.getFont("Tahoma", Font.PLAIN, 11));

		final JFastLabel title = new JFastLabel("                 ");
		title.setPreferredSize(new Dimension(350, 20));
		title.setVerticalAlignment(SwingConstants.BOTTOM);
		title.setBorder(BorderFactory.createEmptyBorder(3,0,0,0));
		title.setFont(FontLib.getFont("Tahoma", Font.PLAIN, 16));

		gview.addControlListener(new ControlAdapter() {
			public void itemEntered(VisualItem item, MouseEvent e) {
				if ( item.canGetString(gview.m_label) )
					title.setText(item.getString(gview.m_label));
			}
			public void itemExited(VisualItem item, MouseEvent e) {
				title.setText(null);
			}
		});


		Box box = new Box(BoxLayout.X_AXIS);
		box.add(Box.createHorizontalStrut(10));
		box.add(title);
		box.add(Box.createHorizontalGlue());
		box.add(search);
		box.add(Box.createHorizontalStrut(3));

		JPanel panel = new JPanel(new BorderLayout());
		panel.add(gview, BorderLayout.CENTER);
		panel.add(box, BorderLayout.SOUTH);

		Color BACKGROUND = Color.WHITE;
		Color FOREGROUND = Color.DARK_GRAY;
		UILib.setColor(panel, BACKGROUND, FOREGROUND);

		return panel;
	}

	// ------------------------------------------------------------------------

	

	/**
	 * Switch the root of the tree by requesting a new spanning tree
	 * at the desired root
	 */
	public static class TreeRootAction extends GroupAction {
		public TreeRootAction(String graphGroup) {
			super(graphGroup);
		}
		public void run(double frac) {
			TupleSet focus = m_vis.getGroup(Visualization.FOCUS_ITEMS);
			if ( focus==null || focus.getTupleCount() == 0 ) return;

			Graph g = (Graph)m_vis.getGroup(m_group);
			Node f = null;
			@SuppressWarnings("rawtypes")
			Iterator tuples = focus.tuples();
			while (tuples.hasNext() && !g.containsTuple(f=(Node)tuples.next()))
			{
				f = null;
			}
			if ( f == null ) return;
			g.getSpanningTree(f);
		}
	}

	/**
	 * Set node fill colors
	 */
	public static class NodeColorAction extends ColorAction {

		public NodeColorAction(String group) {
			super(group, VisualItem.FILLCOLOR, ColorLib.rgba(255,255,255,0));

			add("ingroup('_search_')", ColorLib.rgb(255,190,190));
			add("ingroup('_focus_')", ColorLib.rgb(198,229,229));
		}

	} // end of inner class NodeColorAction

	/**
	 * Set node text colors
	 */
	public static class TextColorAction extends ColorAction {
		public TextColorAction(String group) {
			super(group, VisualItem.TEXTCOLOR, ColorLib.gray(0));
			add("_hover", ColorLib.rgb(255,255,255));
		}
	} // end of inner class TextColorAction


	public static class LinkRenderer extends EdgeRenderer {


		protected String m_labelName = "type";


		public LinkRenderer(int constant1, int constant2){
			super(constant1,constant2);
		}

		@Override
		public void render(Graphics2D g, VisualItem item) {     
			super.render(g, item);
			makeLabel(g,item);
		}



		protected void makeLabel(Graphics2D g, VisualItem item) {

			float x=0, y=0;                

			EdgeItem edge = (EdgeItem)item;

			String label = edge.getString(m_labelName);
			if(label.equals("FROM") || label.equals("TO")){


				VisualItem item1 = edge.getSourceItem();
				VisualItem item2 = edge.getTargetItem();

				// label is positioned to the center of the edge
				x = (float) ((item1.getX()+item2.getX())/2);
				y = (float) ((item1.getY()+item2.getY())/2); 

				Font font = new Font("Verdana", Font.BOLD, 8);
				g.setFont(font);
				g.drawString(label, x, y);

			}

		}



	}


} // end of class RadialGraphView
