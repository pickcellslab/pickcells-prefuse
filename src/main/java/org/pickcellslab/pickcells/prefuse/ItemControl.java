package org.pickcellslab.pickcells.prefuse;

import java.awt.EventQueue;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.tools.MutableInt;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataPointer;
import org.pickcellslab.foundationj.dbm.meta.ManuallyStored;
import org.pickcellslab.foundationj.dbm.meta.MetaCastable;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaFunction;
import org.pickcellslab.foundationj.dbm.meta.MetaKey;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaTraversal;
import org.pickcellslab.foundationj.dbm.queries.actions.Actions;
import org.pickcellslab.foundationj.queryui.utils.MetaToIcon;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.picking.SeriesPickConsumer;
import org.pickcellslab.pickcells.api.app.picking.SeriesPicker;

import net.boplicity.xmleditor.XmlTextPane;
import prefuse.controls.ControlAdapter;
import prefuse.visual.VisualItem;

public class ItemControl extends ControlAdapter implements SeriesPicker {



	private DataAccess session;
	private final UITheme theme;
	private final NotificationFactory notifier;
	private final List<SeriesPickConsumer> lst = new ArrayList<>();


	//private static final AKey<Integer> sourceId = AKey.get("Source ID", Integer.class);
	//private static final AKey<Integer> targetId = AKey.get("Target ID", Integer.class);

	ItemControl(UITheme theme, DataAccess session, NotificationFactory notifier){
		this.theme = theme;
		this.session = session;
		this.notifier = notifier;
	};

	@Override
	public void itemClicked(VisualItem item, MouseEvent e){

		EventQueue.invokeLater(()->{


			if(e.getButton() == MouseEvent.BUTTON3){


				MetaModel mItem = (MetaModel) item.get(MetaModelView.MetaColumn);

				if(mItem == null)
					return;

				// Build popup
				JPopupMenu popOver = new JPopupMenu();



				//TODO eventually should be removed (useful for debugging)
				if(mItem instanceof MetaModel){
					JMenuItem mI = new JMenuItem("Recognition String");
					mI.addActionListener(l->JOptionPane.showMessageDialog(null, mItem.getAttribute(MetaModel.recognition).orElse("Not Available")));
					popOver.add(mI);
				}








				// Register DataPointer to seriesPickers
				if(mItem instanceof DataPointer) {

					for(SeriesPickConsumer c : lst){
						JMenuItem mI = new JMenuItem(c.name());
						mI.addActionListener(l-> new Thread(()->c.seriesPicked(mItem.toString(), (DataPointer) mItem)).start());
						popOver.add(mI);
					}

					// Add a shortcut to get a count of these objects in the database
					JMenuItem mI = new JMenuItem("Show Count");	
					mI.addActionListener(l->{
						new Thread(()->{
							final MutableInt count = new MutableInt();
							try {																	
								((DataPointer) mItem).readData(session, Actions.singleAction(o->count.increment(), "Count"), -1);
							} catch (Exception e1) {
								notifier.display("Error", "Could not count "+mItem.toString(), e1, Level.WARNING);
							}
							EventQueue.invokeLater(()->{
								JOptionPane.showMessageDialog(null, "There are currently "+count.intValue()+" "+mItem.name());
							});
						}).start();	
					});
					popOver.add(mI);

				}







				//Handle the particular case of Filter and Traversals (show XML definition)
				//TODO eventually remove (useful for debugging)
				if(mItem instanceof MetaFilter){
					JMenuItem sItem = new JMenuItem("XML definition");
					popOver.add(sItem);
					sItem.addActionListener(l->{
						JPanel panel = buildXML(mItem.getAttribute(AKey.get("Predicate", String.class)).get());
						JOptionPane.showMessageDialog(null, panel, mItem.toString(), JOptionPane.PLAIN_MESSAGE, MetaToIcon.get(mItem, session.dataRegistry(), theme, 16));
					});
				}else if(mItem instanceof MetaTraversal){
					JMenuItem sItem = new JMenuItem("XML definition");
					popOver.add(sItem);
					sItem.addActionListener(l->{
						JPanel panel = buildXML(mItem.getAttribute(AKey.get("Traversal", String.class)).get());
						JOptionPane.showMessageDialog(null, panel, mItem.toString(), JOptionPane.PLAIN_MESSAGE, MetaToIcon.get(mItem, session.dataRegistry(), theme, 16));
					});
				}
				else if(mItem instanceof MetaFunction){
					JMenuItem sItem = new JMenuItem("XML definition");
					popOver.add(sItem);
					sItem.addActionListener(l->{
						JPanel panel = buildXML(mItem.getAttribute(AKey.get("Function", String.class)).get());
						JOptionPane.showMessageDialog(null, panel, mItem.toString(), JOptionPane.PLAIN_MESSAGE, MetaToIcon.get(mItem, session.dataRegistry(), theme, 16));
					});
				}









				//Allow for deleting the annotation if this was manually stored
				if(mItem instanceof ManuallyStored){
					JMenuItem dItem = new JMenuItem("Delete annotation");
					popOver.add(dItem);
					dItem.addActionListener(l->{
						new Thread(()->{	
							try {																	
								session.queryFactory().deleteAnnotation((ManuallyStored) mItem);
							} catch (Exception e1) {
								notifier.display("Error", "Could not delete "+mItem.toString(), e1, Level.WARNING);
							}
							EventQueue.invokeLater(()->{
								notifier.notify(null, mItem.toString()+ " was deleted successfully");
							});
						}).start();	
					});
				}



				//Allow for casting the annotation if this is a MetaCastable
				if(mItem instanceof MetaCastable){
					JMenuItem dItem = new JMenuItem("Cast annotation");
					popOver.add(dItem);
					dItem.addActionListener(l->{						
						new Thread(()->{	
							try {																	
								((MetaCastable<?>) mItem).cast(session);
							} catch (Exception e1) {
								notifier.display("Error", "Could not cast "+mItem.toString(), e1, Level.WARNING);
								return;
							}
							EventQueue.invokeLater(()->{
								notifier.notify(null, mItem.toString()+ " was cast successfully");
							});
						}).start();												
					});
				}





				// Allow to delete (some) MetaKey
				if(mItem instanceof MetaKey){
					JMenuItem delItem = new JMenuItem("Delete instances");
					popOver.add(delItem);
					delItem.addActionListener(l->{
						//If Other just show confirm dialog
						int i = JOptionPane.showConfirmDialog(null, 
								"Are you sure you want to delete these data ("+mItem+") ?",
								"Deletion",
								JOptionPane.YES_NO_OPTION);

						if(i==JOptionPane.YES_OPTION) {

							AKey<?> k = ((MetaKey<?>) mItem).toAKey();
							if(k == DataItem.idKey){
								JOptionPane.showMessageDialog(null, "It is not possible to remove "+k.name);
								return;
							}	

							new Thread(()->{
								final MutableInt delNumber = new MutableInt();	
								try {																	
									delNumber.set(((MetaKey<?>) mItem).deleteThisAttribute(session));
								} catch (Exception e1) {
									notifier.display("Data Access Error", "An error occured while accessing the data",e1, Level.WARNING);
								}
								EventQueue.invokeLater(()->{
									notifier.notify(null, "Number of deletions "+delNumber.intValue());
								});

							}).start();


						}

					});
				}




				// DataPointers handling
				if(mItem instanceof DataPointer){
					JMenuItem delItem = new JMenuItem("Delete instances");
					popOver.add(delItem);
					delItem.addActionListener(l->{
						//If Other just show confirm dialog
						int i = JOptionPane.showConfirmDialog(null, 
								"Are you sure you want to delete these data ("+mItem+") ?",
								"Deletion",
								JOptionPane.YES_NO_OPTION);

						if(i==JOptionPane.YES_OPTION) {
							new Thread(()->{
								final MutableInt delNumber = new MutableInt();	
								try {																	
									delNumber.set(((DataPointer) mItem).deleteData(session));
								} catch (Exception e1) {
									notifier.display("Data Access Error", "An error occured while accessing the data",e1, Level.WARNING);
								}
								EventQueue.invokeLater(()->{
									notifier.notify(null, "Number of deletions "+delNumber.intValue());
								});

							}).start();
						}
					});      
				}

				popOver.show(e.getComponent(), e.getX(), e.getY());


			}

		});

	}







	private JPanel buildXML(String xml) {
		final JPanel panel = new JPanel();
		XmlTextPane pane = new XmlTextPane();
		pane.setText(xml);
		panel.add(pane);
		return panel;
	}










	@Override
	public void addSeriesPickListener(SeriesPickConsumer l) {
		lst.add(l);
	}

	@Override
	public void removeSeriesPickListener(SeriesPickConsumer l) {
		lst.remove(l);
	}







}
