package org.pickcellslab.pickcells.prefuse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.StepTraverser;
import org.pickcellslab.foundationj.datamodel.tools.TraversalStep;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaInfo;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import prefuse.data.Edge;
import prefuse.data.Graph;
import prefuse.data.Node;
import prefuse.data.Table;
import prefuse.data.Tree;

/**
 * Translate a MetaModel into a prefuse Graph
 * @author Guillaume
 *
 */
public class MetaToPrefuse {

	private static Logger log = LoggerFactory.getLogger(MetaToPrefuse.class);


	public static Tree makeTree(MetaInfo metaInfo){

		//Init the Tree
		Tree tree = new Tree();    
		Table nodeTable = tree.getNodeTable();
		nodeTable.addColumn("name", String.class);

		Optional<MetaModel> rootOpt = metaInfo.items().stream().filter(i->{
			if(!MetaClass.class.isAssignableFrom(i.getClass()))
				return false;
			if(((MetaClass)i).itemDeclaredType().equals(DataNode.class.getTypeName()))
				return true;
			return false;
		}).findFirst();

		MetaModel root = null;
		if(rootOpt.isPresent())
			root = rootOpt.get();
		else throw new IllegalStateException("No root found in the MetaModel!");

		log.debug("optional is present : "+rootOpt.isPresent());
		Node rootNode = tree.addRoot();
		rootNode.setString("name", root.toString());



		//New traversal from root
		StepTraverser<NodeItem,Link> t = Traversers.breadthFirst(root);

		//Traversal------------------------------------------

		Map<WritableDataItem, Node> sources = new LinkedHashMap<>();
		Map<WritableDataItem, Node> targets = new LinkedHashMap<>();
		List<Link> linksToStore = new ArrayList<>();


		TraversalStep<NodeItem,Link> step = null;
		while ((step = t.nextStep()) != null) {

			if(step.round == 0){
				//Add root
				targets.put(root, rootNode);

			}else{

				log.debug("Step : "+ step.round);

				for(WritableDataItem i : step.nodes){
					Node n = tree.addNode();
					targets.put(i,n);
					n.setString("name", i.toString());;
				}

				for(Link l : linksToStore){
					// creating the Link
					// get Source node and target node
					Node source = sources.get(l.source());
					Node target = targets.get(l.target());

					if(source== null || target == null){// Traverser returns both incoming and outgoing links
						//Try other direction
						log.debug("trying other direction");
						source = targets.get(l.source());
						target = sources.get(l.target());

						if(source== null || target == null){//selfReferencing or loop! cannot be added to a tree
							log.debug("self ref or loop identified");
							continue;           
						}
						log.debug("Adding child relationship ->"+l.target().toString());
						log.debug("Adding child relationship ->"+l.source().toString());
						tree.addChildEdge(target, source);

					}else{

						log.debug("Adding child relationship ->"+l.source().toString());
						log.debug("Adding child relationship ->"+l.target().toString());
						tree.addChildEdge(source, target);
					}
				}
			}
			sources.putAll(targets);
			targets.clear();
			linksToStore = step.edges;

		}


		return tree;

	}



	public static Graph makeGraph(MetaInfo metaInfo){
		return makeGraph(metaInfo.items().get(0));  
	}



	public static void update(Graph graph, Collection<? extends MetaModel> meta){
		
		//First add all the new nodes
		for(MetaModel i : meta){
			Node n = graph.addNode();			
			n.setString("name", i.toString());
			n.setString("type", i.getClass().getTypeName());
			n.setString("description", i.toHTML());
		}
				
		//Now create the edges
	}


	public static Graph makeGraph(MetaModel root){

		//Init the Tree
		Graph graph = new Graph(true);    
		Table nodeTable = graph.getNodeTable();
		nodeTable.addColumn("name", String.class);
		nodeTable.addColumn("type", String.class);
		nodeTable.addColumn("description", String.class);

		Table edgeTable = graph.getEdgeTable();
		edgeTable.addColumn("type", String.class);

		//New traversal from root
		StepTraverser<NodeItem, Link> t = Traversers.breadthFirst(root);

		//Traversal------------------------------------------

		Map<WritableDataItem, Node> sources = new LinkedHashMap<>();
		Map<WritableDataItem, Node> targets = new LinkedHashMap<>();
		List<Link> linksToStore = new ArrayList<>();


		TraversalStep<NodeItem,Link> step = null;
		while ((step = t.nextStep()) != null) {


			log.debug("Step : "+ step.round);

			for(WritableDataItem i : step.nodes){
				Node n = graph.addNode();				
				targets.put(i,n);         
				n.setString("name", i.toString());
				n.setString("type", i.getClass().getTypeName());
				n.setString("description", ((MetaModel)i).toHTML());
			}

			for(Link l : linksToStore){
				// creating the Link
				// get Source node and target node
				Node source = sources.get(l.source());
				Node target = targets.get(l.target());

				if(source== null || target == null){// Traverser returns both incoming and outgoing links
					target = targets.get(l.target());
					source = targets.get(l.source());
					target = sources.get(l.target());

					if(source == null)//selfReferencing!
						source = sources.get(l.source());
					else if(target == null)
						target = targets.get(l.target());
				}

				log.debug("Adding child relationship ->"+l.source().toString());
				log.debug("Adding child relationship ->"+l.target().toString());
				Edge e = graph.addEdge(source, target);
				e.setString("type", l.declaredType());
			}



			sources.putAll(targets);
			targets.clear();
			linksToStore = step.edges;
		}

		return graph;

	}




}
