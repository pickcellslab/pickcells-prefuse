package org.pickcellslab.pickcells.prefuse.treeview;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;

import prefuse.visual.VisualItem;
import prefuse.visual.sort.ItemSorter;

public class DimensionalItemSorter extends ItemSorter{


	private Dimension<DataItem, Number> dim;
	private String dataField;


	public DimensionalItemSorter(Dimension<DataItem, Number> dim, String dataField) {
		assert dim != null;
		this.dim = dim;
		this.dataField = dataField;
	}


	@Override
	public int score(VisualItem item) {
		final DataItem i = (DataItem) item.get(dataField);
		if(i==null)
			return 0;		
		else
			return dim.apply(i).intValue();
	}

}
