package org.pickcellslab.pickcells.prefuse.treeview;

import java.awt.BorderLayout;
import java.net.URL;
import java.util.function.Predicate;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer.Mode;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dataviews.mvc.GraphSequenceViewModel;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.queries.config.QueryConfigs;
import org.pickcellslab.foundationj.dbm.queries.config.QueryWizardConfig;
import org.pickcellslab.foundationj.dbm.sequences.GraphSequencePointer;
import org.pickcellslab.pickcells.api.app.charts.DBViewFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.charts.ViewFactoryException;
import org.pickcellslab.pickcells.api.app.modules.DefaultUIDocument;
import org.pickcellslab.pickcells.api.app.modules.UIDocument;



@Module
public class TreeViewFactory implements DBViewFactory{

	
	
	
	
	@Override
	public String description() {
		return "Displays trees for Series Generators types";
	}

	@Override
	public String name() {
		return "Tree View";
	}


	@Override
	public ViewType getType() {
		return ViewType.Numerical;
	}
	
	
	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/lineages_icon.png"));
	}
	
	
	@Override
	public UIDocument createView(DataAccess access, DBViewHelp helper) throws ViewFactoryException {
		

		//target predicate
				final Predicate<DimensionContainer<DataItem, ?>> p = dc->dc.getRaw().dataType()==dType.NUMERIC;

				//Create the model 
				final GraphSequenceViewModel<Path<NodeItem, Link>, Number> model = new GraphSequenceViewModel<>(access, Mode.DECOMPOSED, p);

				//Create the scatter
				final TreeView tv = new TreeView(model, helper);


				QueryWizardConfig<DataItem, ?> config = QueryConfigs.newTargetedOnlyConfig("")
						.setAllowedQueryables(mq->{
							if(mq instanceof MetaClass)
								return GraphSequencePointer.class.isAssignableFrom(((MetaClass) mq).itemClass(access.dataRegistry()));
							else
								return false;				 
						}).build();


				// Create Controllers
				JPanel toolBar = helper.newToolBar()		
						.addChangeDataSetButton(() -> config, tv.getModel())
						.addChangeDimensionsButton(tv)
						.build();


				JPanel scene = new JPanel();
				scene.setLayout(new BorderLayout());
				scene.add(toolBar, BorderLayout.NORTH);
				scene.add(new JScrollPane(tv.view()), BorderLayout.CENTER);

				return new DefaultUIDocument(scene, name(), icon());
		
		
		
	}

	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}

	

}
