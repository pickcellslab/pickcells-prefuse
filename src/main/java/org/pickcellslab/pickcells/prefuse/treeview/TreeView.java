package org.pickcellslab.pickcells.prefuse.treeview;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import javax.swing.JPanel;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dataviews.mvc.DataSet;
import org.pickcellslab.foundationj.dataviews.mvc.GraphSequenceConsumerFactory;
import org.pickcellslab.foundationj.dataviews.mvc.GraphSequenceViewModel;
import org.pickcellslab.foundationj.dataviews.mvc.ReconfigurableView;
import org.pickcellslab.foundationj.dataviews.mvc.RedefinableBrokerContainer;
import org.pickcellslab.foundationj.dataviews.mvc.SeriesGeneratorBroker;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.picking.AbstractTotiPicker;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;

import prefuse.data.Edge;
import prefuse.data.Node;
import prefuse.data.Table;
import prefuse.data.Tree;

public class TreeView extends AbstractTotiPicker<WritableDataItem> 
implements ReconfigurableView<
GraphSequenceViewModel<Path<NodeItem,Link>,Number>,
DataItem,
SeriesGeneratorBroker<Number>,
Number>{


	static final String idField = "ID", dataField = "DATA", coordField = "COORD";
;


	private GraphSequenceViewModel<Path<NodeItem, Link>, Number> model;

	private String currentQuery;
	private Dimension<DataItem, Number> currentDimension;

	private final Set<Integer> visitedGens = new HashSet<>();
	private final Map<Integer, Edge> linkMap = new HashMap<>();
	private final Map<Integer, Node> nodeMap = new HashMap<>();


	private final JPanel view = new JPanel();
	private Tree tree;

	public TreeView(GraphSequenceViewModel<Path<NodeItem,Link>, Number> model, DBViewHelp helper) {

		this.model = model;

		// Initialise the view
		view.setName("Tree View");







		//Register listeners	

		//DataSet updates
		model.addDataEvtListener(this);

		// Dimensions updates
		model.addBrokerChangedListener(this);
		helper.registerSingleConsumersFor(this);
		helper.registerDistributionConsumerFor(this);


	}









	@Override
	public void setModel(GraphSequenceViewModel<Path<NodeItem, Link>, Number> model) {
		if(model!=null){
			model.removeDataEvtListener(this);			
		}		
		this.model = model;
		model.addBrokerChangedListener(this);
	}

	@Override
	public GraphSequenceViewModel<Path<NodeItem, Link>, Number> getModel() {
		return model;
	}

	@Override
	public void dataSetChanged(DataSet<DataItem> oldDataSet, DataSet<DataItem> newDataSet) {

		view.removeAll();		
		view.setLayout(new BorderLayout());

		visitedGens.clear();
		nodeMap.clear();
		linkMap.clear();

		tree = new Tree();
		Table nodeTable = tree.getNodeTable();
		nodeTable.addColumn(idField, int.class);
		nodeTable.addColumn(dataField, double.class); 
		nodeTable.addColumn(coordField, double.class); 

		Table edgeTable = tree.getEdgeTable();
		edgeTable.addColumn(idField, int.class);
		edgeTable.addColumn(dataField, double.class);

		final Node rootNode = tree.addRoot();
		rootNode.set(idField, -1);
		rootNode.setDouble(dataField, 0);
		nodeMap.put(-1, rootNode);

		currentQuery = newDataSet.queryID(0);

		view.add("Center", TreeDisplay.createView(tree, dataField));
		view.revalidate();
		view.repaint();

	}




	@Override
	public void brokerChanged(RedefinableBrokerContainer<DataItem, SeriesGeneratorBroker<Number>, Number> source,
			String query, SeriesGeneratorBroker<Number> old, SeriesGeneratorBroker<Number> broker) {


		//load the data	
		if(broker != null){
			try {
				currentDimension = broker.dimension(0);
				load();
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}		


	}








	private void load() throws DataAccessException {
		for(int q = 0; q<model.getDataSet().numQueries(); q++)			
			model.consumeQuery(q, new PathConsumerFactory(currentDimension));
	}






	public Component view() {
		return view;
	}





	@Override
	public String getQueryUnderFocus() {
		return currentQuery;
	}

	@Override
	public int numDimensionalAxes() {
		return 1;
	}

	@Override
	public String axisName(int index) {
		return "Time";
	}

	@Override
	public Dimension<DataItem, Number> currentAxisDimension(int index) {
		return currentDimension;
	}






	private class PathConsumerFactory implements GraphSequenceConsumerFactory{

		private final Dimension<DataItem, ?> yDim;

		public PathConsumerFactory(final  Dimension<DataItem,Number> yDim) {
			this.yDim = yDim;
		}


		@Override
		public Consumer<Path<NodeItem, Link>> createConsumer(String t, int u) {
			return p->{

				if(visitedGens.add(u)){
					Node first = createIfInexistant(p.first());
					Edge visEdge = tree.addChildEdge(nodeMap.get(-1), first);
					Object y = yDim.apply(p.first());
					if(y!=null){
						if(y instanceof Number){
							if(!Double.isNaN(((Number) y).doubleValue()))
								first.set(dataField, y);
						}
						else
							first.set(dataField, y);
					}

				}

				final NodeItem n = p.last();				
				final Node visNode = createIfInexistant(n);
				
				
				final Link l = p.lastEdge();
				Edge visEdge = linkMap.get(l.getAttribute(DataItem.idKey).get());
				if(visEdge == null){
					visEdge = tree.addChildEdge(createIfInexistant(l.source()), createIfInexistant(l.target()));
					visEdge.set(idField, n.getAttribute(DataItem.idKey).get());
					linkMap.put(l.getAttribute(DataItem.idKey).get(), visEdge);
				}



				Object y = yDim.apply(n);
				if(y==null)
					y = yDim.apply(l);
				if(y!=null){	
					if(y instanceof Number){
						if(!Double.isNaN(((Number) y).doubleValue()))
							visNode.set(dataField, y);
					}
					else
						visNode.set(dataField, 0);
				}			
			};
		}


		private Node createIfInexistant(NodeItem n){
			Node visNode = nodeMap.get(n.getAttribute(DataItem.idKey).get());
			if(visNode == null){
				visNode = tree.addNode();
				visNode.set(idField, n.getAttribute(DataItem.idKey).get());
				nodeMap.put(n.getAttribute(DataItem.idKey).get(), visNode);
				visNode.setDouble(coordField, n.getAttribute(ImageLocated.frameKey).orElse(1));
			}
			return visNode;
		}


	}





}
