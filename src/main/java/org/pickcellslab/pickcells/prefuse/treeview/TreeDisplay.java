package org.pickcellslab.pickcells.prefuse.treeview;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.DefaultNodeItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.StepTraverser;
import org.pickcellslab.foundationj.datamodel.tools.TraversalStep;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;

import prefuse.Constants;
import prefuse.Display;
import prefuse.Visualization;
import prefuse.action.ActionList;
import prefuse.action.ItemAction;
import prefuse.action.RepaintAction;
import prefuse.action.animate.ColorAnimator;
import prefuse.action.animate.PolarLocationAnimator;
import prefuse.action.animate.QualityControlAnimator;
import prefuse.action.animate.VisibilityAnimator;
import prefuse.action.assignment.ColorAction;
import prefuse.action.assignment.DataColorAction;
import prefuse.action.assignment.FontAction;
import prefuse.action.layout.CollapsedSubtreeLayout;
import prefuse.activity.SlowInSlowOutPacer;
import prefuse.controls.ControlAdapter;
import prefuse.controls.FocusControl;
import prefuse.controls.HoverActionControl;
import prefuse.controls.PanControl;
import prefuse.controls.ZoomControl;
import prefuse.controls.ZoomToFitControl;
import prefuse.data.Edge;
import prefuse.data.Node;
import prefuse.data.Table;
import prefuse.data.Tree;
import prefuse.data.Tuple;
import prefuse.data.event.TupleSetListener;
import prefuse.data.query.SearchQueryBinding;
import prefuse.data.search.PrefixSearchTupleSet;
import prefuse.data.search.SearchTupleSet;
import prefuse.data.tuple.DefaultTupleSet;
import prefuse.data.tuple.TupleSet;
import prefuse.render.AbstractShapeRenderer;
import prefuse.render.DefaultRendererFactory;
import prefuse.render.EdgeRenderer;
import prefuse.render.ShapeRenderer;
import prefuse.util.ColorLib;
import prefuse.util.FontLib;
import prefuse.util.ui.JFastLabel;
import prefuse.util.ui.JSearchPanel;
import prefuse.util.ui.UILib;
import prefuse.visual.VisualItem;
import prefuse.visual.expression.InGroupPredicate;
import prefuse.visual.sort.TreeDepthItemSorter;


@SuppressWarnings("serial")
public class TreeDisplay extends Display {


	private static final String tree = "tree";
	private static final String treeNodes = "tree.nodes"; 
	private static final String treeEdges = "tree.edges";
	private static final String linear = "linear";

	private ShapeRenderer m_nodeRenderer;
	private EdgeRenderer m_edgeRenderer;


	public TreeDisplay(Tree g, String dataField) {
		
		super(new Visualization());
		

		// -- set up visualization --
		m_vis.add(tree, g);
		m_vis.setInteractive(treeEdges, null, false);

		// -- set up renderers --
		m_nodeRenderer = new ShapeRenderer(50);
		m_nodeRenderer.setRenderType(AbstractShapeRenderer.RENDER_TYPE_DRAW_AND_FILL);
		//m_nodeRenderer.setHorizontalAlignment(Constants.LEFT);
		//m_nodeRenderer.setManageBounds(true);
		//m_nodeRenderer.setRoundedCorner(20,20);
		//m_nodeRenderer.setHorizontalPadding(5);
		//m_nodeRenderer.setVerticalPadding(5);
		m_edgeRenderer = new EdgeRenderer();
		m_edgeRenderer.setDefaultLineWidth(20);
		//m_edgeRenderer.setEdgeType(Constants.EDGE_TYPE_CURVE); 
		
		
		
		DefaultRendererFactory rf = new DefaultRendererFactory(m_nodeRenderer);
		rf.add(new InGroupPredicate(treeEdges), m_edgeRenderer);
		m_vis.setRendererFactory(rf);

		// -- set up processing actions --

		// colors
		ItemAction outlineColor = new NodeColorAction(treeNodes);
		ItemAction nodeColor = new DataColorAction(treeNodes, dataField, Constants.NUMERICAL, VisualItem.FILLCOLOR, ColorLib.getHotPalette());
		ItemAction textColor = new TextColorAction(treeNodes);
		m_vis.putAction("textColor", textColor);

		ItemAction edgeColor = new ColorAction(treeEdges,
				VisualItem.STROKECOLOR, ColorLib.rgb(200,200,200));

		FontAction fonts = new FontAction(treeNodes, 
				FontLib.getFont("Tahoma", 10));
		
		fonts.add("ingroup('_focus_')", FontLib.getFont("Tahoma", 11));

		
				
		
		// recolor
		ActionList recolor = new ActionList();
		recolor.add(nodeColor);
		recolor.add(outlineColor);
		
		recolor.add(textColor);
		m_vis.putAction("recolor", recolor);

		// repaint
		ActionList repaint = new ActionList();
		repaint.add(recolor);
		repaint.add(new RepaintAction());
		m_vis.putAction("repaint", repaint);

		// animate paint change
		ActionList animatePaint = new ActionList(400);
		animatePaint.add(new ColorAnimator(treeNodes));
		animatePaint.add(new RepaintAction());
		m_vis.putAction("animatePaint", animatePaint);

		// create the tree layout action
		LineageTreeLayout treeLayout = new LineageTreeLayout(tree, TreeView.coordField);
		treeLayout.setSubtreeSpacing(400);
		treeLayout.setBreadthSpacing(50);
		treeLayout.setDepthSpacing(10);
		
		//treeLayout.setAngularBounds(-Math.PI/2, Math.PI);
		m_vis.putAction("treeLayout", treeLayout);

		CollapsedSubtreeLayout subLayout = new CollapsedSubtreeLayout(tree);
		m_vis.putAction("subLayout", subLayout);

		// create the filtering and layout
		ActionList filter = new ActionList();
		//filter.add(new TreeRootAction(tree));
		filter.add(fonts);
		filter.add(subLayout);
		filter.add(treeLayout);
		
		filter.add(textColor);
		filter.add(nodeColor);
		filter.add(edgeColor);
		
		m_vis.putAction("filter", filter);

		// animated transition
		ActionList animate = new ActionList(1250);
		animate.setPacingFunction(new SlowInSlowOutPacer());
		animate.add(new QualityControlAnimator());
		animate.add(new VisibilityAnimator(tree));
		animate.add(new PolarLocationAnimator(treeNodes, linear));
		animate.add(new ColorAnimator(treeNodes));
		animate.add(new RepaintAction());
		m_vis.putAction("animate", animate);
		m_vis.alwaysRunAfter("filter", "animate");

		// ------------------------------------------------

		// initialize the display
		setSize(600,600);
		setItemSorter(new TreeDepthItemSorter());
		//addControlListener(new DragControl());
		addControlListener(new ZoomToFitControl());
		addControlListener(new ZoomControl());
		addControlListener(new PanControl());
		addControlListener(new FocusControl(1, "filter"));
		addControlListener(new HoverActionControl("repaint"));

		// ------------------------------------------------

		// filter graph and perform layout
		m_vis.run("filter");

		// maintain a set of items that should be interpolated linearly
		// this isn't absolutely necessary, but makes the animations nicer
		// the PolarLocationAnimator should read this set and act accordingly
		m_vis.addFocusGroup(linear, new DefaultTupleSet());
		m_vis.getGroup(Visualization.FOCUS_ITEMS).addTupleSetListener(
				new TupleSetListener() {
					public void tupleSetChanged(TupleSet t, Tuple[] add, Tuple[] rem) {
						TupleSet linearInterp = m_vis.getGroup(linear);
						if ( add.length < 1 ) return; linearInterp.clear();
						for ( Node n = (Node)add[0]; n!=null; n=n.getParent() )
							linearInterp.addTuple(n);
					}
				}
				);

		SearchTupleSet search = new PrefixSearchTupleSet();
		m_vis.addFocusGroup(Visualization.SEARCH_ITEMS, search);
		search.addTupleSetListener(new TupleSetListener() {
			public void tupleSetChanged(TupleSet t, Tuple[] add, Tuple[] rem) {
				m_vis.cancel("animatePaint");
				m_vis.run("recolor");
				m_vis.run("animatePaint");
			}
		});
	}

	
	
	
	
	
	
	
	
	
	
	
	// ------------------------------------------------------------------------

	public static void main(String argv[]) {
		
		String label = "data";


		UILib.setPlatformLookAndFeel();

		JFrame frame = new JFrame("Demo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(createView(createTestTree(), label));
		frame.pack();
		frame.setVisible(true);
	}

	

	public static JPanel createView(Tree g, final String dataField) {        
		// create a new radial tree view
		final TreeDisplay gview = new TreeDisplay(g, dataField);
		Visualization vis = gview.getVisualization();

		// create a search panel for the tree map
		SearchQueryBinding sq = new SearchQueryBinding(
				(Table)vis.getGroup(treeNodes), dataField,
				(SearchTupleSet)vis.getGroup(Visualization.SEARCH_ITEMS));
		
		JSearchPanel search = sq.createSearchPanel();
		search.setShowResultCount(true);
		search.setBorder(BorderFactory.createEmptyBorder(5,5,4,0));
		search.setFont(FontLib.getFont("Tahoma", Font.PLAIN, 11));

		final JFastLabel title = new JFastLabel("                 ");
		title.setPreferredSize(new Dimension(350, 20));
		title.setVerticalAlignment(SwingConstants.BOTTOM);
		title.setBorder(BorderFactory.createEmptyBorder(3,0,0,0));
		title.setFont(FontLib.getFont("Tahoma", Font.PLAIN, 16));

		gview.addControlListener(new ControlAdapter() {
			public void itemEntered(VisualItem item, MouseEvent e) {
				if ( item.canGetString(dataField) )
					title.setText("Frame = "+item.getDouble(TreeView.coordField)+" - Current Value: "+item.getString(dataField));
			}
			public void itemExited(VisualItem item, MouseEvent e) {
				title.setText(null);
			}
		});

		Box box = new Box(BoxLayout.X_AXIS);
		box.add(Box.createHorizontalStrut(10));
		box.add(title);
		box.add(Box.createHorizontalGlue());
		box.add(search);
		box.add(Box.createHorizontalStrut(3));

		JPanel panel = new JPanel(new BorderLayout());
		panel.add(gview, BorderLayout.CENTER);
		panel.add(box, BorderLayout.SOUTH);

		Color BACKGROUND = Color.WHITE;
		Color FOREGROUND = Color.DARK_GRAY;
		UILib.setColor(panel, BACKGROUND, FOREGROUND);

		return panel;
	}



	/**
	 * Set node fill colors
	 */
	public static class NodeColorAction extends ColorAction {
		public NodeColorAction(String group) {
			super(group, VisualItem.STROKECOLOR, ColorLib.rgba(255,255,255,0));
			add("_hover", ColorLib.gray(35,230));
			add("ingroup('_search_')", ColorLib.rgb(255,190,190));
			add("ingroup('_focus_')", ColorLib.rgb(198,229,229));
		}

	} // end of inner class NodeColorAction

	/**
	 * Set node text colors
	 */
	public static class TextColorAction extends ColorAction {
		public TextColorAction(String group) {
			super(group, VisualItem.TEXTCOLOR, ColorLib.gray(0));
			add("_hover", ColorLib.gray(35,230));
		}
	} // end of inner class TextColorAction




	private static AKey<Double> score = AKey.get("Score", Double.class);

	private static NodeItem newTestNode(String name, double score, int frame){
		NodeItem n = new DefaultNodeItem(name);
		n.setAttribute(TreeDisplay.score, score);
		n.setAttribute(ImageLocated.frameKey, frame);
		return n;
	}
	
	
	private static Tree createTestTree(){

		NodeItem root = newTestNode("A", 0.5, 1);	NodeItem B = newTestNode("B", 2, 2);	NodeItem C = newTestNode("C", 3, 3);	NodeItem D = newTestNode("D", 5, 4);		
		NodeItem E = newTestNode("E", 4, 3);	NodeItem F = newTestNode("F", 4.5, 4);	NodeItem G = newTestNode("G", 7.2, 5);	NodeItem H = newTestNode("H", 8.5, 6);

		new DataLink("TRACK", root, B, true);	new DataLink("TRACK", B, C, true);	new DataLink("TRACK", C, D, true);
		new DataLink("TRACK", B, E, true);	new DataLink("TRACK", E, F, true);	new DataLink("TRACK", F, G, true);	new DataLink("TRACK", G, H, true);

		NodeItem root2 = newTestNode("AA", 0.5, 1);	NodeItem BB = newTestNode("BB", 2, 2);	NodeItem CC = newTestNode("CC", 3, 3);	NodeItem DD = newTestNode("DD", 5, 4);
		NodeItem EE = newTestNode("EE", 4, 3);	NodeItem FF = newTestNode("FF", 4.5, 7);	NodeItem GG = newTestNode("GG", 1, 8);	NodeItem HH = newTestNode("HH", 8.5, 10);

		new DataLink("TRACK", root2, BB, true);	new DataLink("TRACK", BB, CC, true);	new DataLink("TRACK", CC, DD, true);
		new DataLink("TRACK", BB, EE, true);	new DataLink("TRACK", EE, FF, true);	new DataLink("TRACK", FF, GG, true);	new DataLink("TRACK", GG, HH, true);

		final List<NodeItem> roots = new ArrayList<>();
		roots.add(root);
		roots.add(root2);

		return createTree(roots);
	}




	private static Tree createTree(List<NodeItem> roots){

		Tree tree = new Tree();    
		Table nodeTable = tree.getNodeTable();
		nodeTable.addColumn("name", String.class);
		nodeTable.addColumn("data", double.class); 
		nodeTable.addColumn(TreeView.coordField, double.class);
		
		Table edgeTable = tree.getEdgeTable();


		Node rootNode = tree.addRoot();
		rootNode.setString("name", "");
		//rootNode.setDouble("data", 0d); 
		

		for(NodeItem root : roots){

			//New traversal from root
			StepTraverser<NodeItem,Link> t = Traversers.breadthFirst(root);

			//Traversal------------------------------------------

			final Map<DataItem, Node> prefuseNodes = new LinkedHashMap<>();
			
			final Node realRoot = tree.addNode();
			realRoot.setString("name", root.toString());
			realRoot.set("data", 0d);
			realRoot.setDouble(TreeView.coordField, 0);
			prefuseNodes.put(root, realRoot);
			
			final Edge e = tree.addChildEdge(rootNode, realRoot);

			TraversalStep<NodeItem,Link> step = null;
			while ((step = t.nextStep()) != null) {

				for(Link l : step.edges){

					System.out.println("Testing Link "+l);

					// creating the Link
					// get Source node and target node
					Node source = prefuseNodes.get(l.source());
					if(source==null){
						source = tree.addNode();
						source.setString("name", l.source().toString());
						source.setDouble("data", l.source().getAttribute(score).orElse(0d));
						source.setDouble(TreeView.coordField, l.source().getAttribute(ImageLocated.frameKey).orElse(0));
						prefuseNodes.put(l.source(), source);
					}
					Node target = prefuseNodes.get(l.target());
					if(target==null){
						target = tree.addNode();
						target.setString("name", l.target().toString());
						target.setDouble("data", l.target().getAttribute(score).orElse(0d));
						target.setDouble(TreeView.coordField, l.target().getAttribute(ImageLocated.frameKey).orElse(0));
						prefuseNodes.put(l.target(), target);
					}


					System.out.println(tree.addChildEdge(source, target)+" added to tree");
				}

			}

		}

		return tree;
	}




} // end of class RadialGraphView
