package org.pickcellslab.pickcells.prefuse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.MutableInt;
import org.pickcellslab.foundationj.datamodel.tools.StepTraverser;
import org.pickcellslab.foundationj.datamodel.tools.TraversalStep;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import prefuse.data.Edge;
import prefuse.data.Graph;
import prefuse.data.Node;
import prefuse.data.Table;

public class MetaToPrefuseGraph {

	private final Logger log = LoggerFactory.getLogger(MetaToPrefuseGraph.class);

	private final Map<WritableDataItem, Node> mapping = new LinkedHashMap<>();


	private Graph graph;

	MetaToPrefuseGraph(){

		//Init the Tree
		graph = new Graph(true);    
		Table nodeTable = graph.getNodeTable();
		nodeTable.addColumn("name", String.class);
		nodeTable.addColumn("type", String.class);
		nodeTable.addColumn(MetaModelView.MetaColumn, MetaModel.class);
		nodeTable.addColumn("description", String.class);

		Table edgeTable = graph.getEdgeTable();
		edgeTable.addColumn("type", String.class);


	}

	public void traverse(MetaModel root){


		log.trace("Traversing from :" + root);

		//New traversal from root
		StepTraverser<NodeItem, Link> t = Traversers.breadthFirst(root);

		//Traversal------------------------------------------
		List<Link> links = new ArrayList<>();

		TraversalStep<NodeItem,Link> step = null;
		while ((step = t.nextStep()) != null) {

			for(NodeItem i : step.nodes){

				try{
					log.trace("Current item = "+i);
					log.trace("Degree ="+(i).links().count()); 

					Node n = graph.addNode();				

					n.setString("name", i.toString());
					n.setString("type", i.getClass().getTypeName());
					n.set(MetaModelView.MetaColumn, i);
					n.setString("description", ((MetaModel) i).toHTML());


					mapping.put(i,n);
				}
				catch(Exception e){
					log.error("MetaModel object badly defined : "+i.toString(), e);
				}
			}



			for(Link l : links){
				try{
					// creating the Link
					// get Source node and target node
					Node source = mapping.get(l.source());
					Node target = mapping.get(l.target());
					Edge e = graph.addEdge(source, target);
					e.setString("type", l.declaredType());

				}catch(Exception e){
					log.error("Link not added : "+l.toString());
				}
			}


			links = step.edges;
		}

		for(Link l : links){
			// creating the Link
			// get Source node and target node
			Node source = mapping.get(l.source());
			Node target = mapping.get(l.target());
			Edge e = graph.addEdge(source, target);
			e.setString("type", l.declaredType());
		}
	}


	public void update(Collection<? extends MetaModel> meta){

		List<MetaModel> newNodes = new ArrayList<>();

		//First add all the new nodes
		for(MetaModel i : meta){

			if(!mapping.containsKey(i)){

				log.trace("Inserting:" + i);
				//if(!mapping.containsKey(i)){
				Node n = graph.addNode();			
				n.setString("name", i.toString());
				n.setString("type", i.getClass().getTypeName());
				n.set(MetaModelView.MetaColumn, i);
				n.setString("description", i.toHTML());
				mapping.put(i,n);   
				newNodes.add(i);
			}
		}

		//Now create the edges
		for(MetaModel i : newNodes){
			final MutableInt c = new MutableInt();
			final Node n = mapping.get(i);
			i.getLinks(Direction.OUTGOING).forEach(l->{
				// creating the Link
				// get Source node and target node	
				log.trace("Inserting:" + l);
				Node target = mapping.get(l.target());
				Edge e = graph.addEdge(n, target);
				e.setString("type", l.declaredType());
				c.increment();
			});
			if (c.intValue() == 0)
				i.getLinks(Direction.INCOMING).forEach(l->{
					// creating the Link
					// get Source node and target node

					Node source = mapping.get(l.source());				

					Edge e = graph.addEdge(source, n);
					e.setString("type", l.declaredType());

				});
		}
	}


	public Graph getPrefuseGraph(){
		return graph;
	}

	public void delete(Collection<MetaModel> meta) {
		for(MetaModel mm : meta){
			System.out.println("MetaToPrefuse : removing "+mm);

			Node n = mapping.remove(mm);
			if(n!=null){
				System.out.println("MetaToPrefuse : mapping found -> removal success");			
				graph.removeNode(n);
			}
		}
	}


}
